{
  "oxnas"."ox820" = {
    sha256 = "1hwp03hkvjkbbqxy0g2a9ks43jjvffbmx0b4648x4bwln06ngzwy";
    feedsSha256.base = "0859fi6iczmbm9iyihml0kvm52b4z46vz3f8wqrqjaif32pynd2w";
    feedsSha256.packages = "11f3irb4ri13nj90nqkhdvswbx6v4sw86kappgck4zql6z0cfzqz";
    feedsSha256.routing = "0xi78p94llbz0g9ysfhhzlakfr6xny12siyx3v6h676adghs6yby";
    feedsSha256.telephony = "0q5vm6jfc3sd9idyll2514v4jmxaawbnq12n0b5ssldqm0a8mjnm";
  };
  "mxs"."generic" = {
    sha256 = "1kkv51vb0qypcjqwlgk5a5frjwx4lkzk7qwzfnr1vxh1vyhddqgm";
  };
  "zynq"."generic" = {
    sha256 = "02ppw08pkdc3v5ngpg9sx73n0cdmihax6li73g8ay46x9znvikav";
    feedsSha256.base = "18j34bcl0wwbg3kbbv5bpf6k3kcj31rwcpqljgqz09sbg6p4fm53";
    feedsSha256.packages = "0a7n5rnvv2y075v7h085sq6r65i0mzlnz9f2xch429qq2yrv04fs";
    feedsSha256.routing = "10gzaa5jvlpr4fbqhah8h2ag0ybma0fby54yyd115fx782s3pmv6";
    feedsSha256.telephony = "12v8w8ba85s53gh88mb66m7jyvk1dvmvvhk79n79wplpwp1armsk";
  };
  "bcm63xx"."generic" = {
    sha256 = "0sfn7035j3g25i3fj3j5qf8f8b8rs441rhmiwpd18fwqmwyxj4dx";
    feedsSha256.base = "1ym8chhsjn9qyms5bncbbayiy3gjr2xsv4a6359pxkr49bc2bikg";
    feedsSha256.packages = "0yr94bcml2zy856wd1w7c4yvb2159x3slrfq2qldpai4w7552xld";
    feedsSha256.routing = "16h642wndffbkhhbc62p1jjw37wzbylb455f659kj7qsxh6wrm0b";
    feedsSha256.telephony = "1hamds3bzw87c3iiap3j38h41rcaz0qk0sc6902y3bqlrq60scys";
  };
  "bcm63xx"."smp" = {
    sha256 = "06frajj5038lgvq437735vhrkdyi900hpy90703l74qqsvd1fyb5";
    feedsSha256.base = "1ym8chhsjn9qyms5bncbbayiy3gjr2xsv4a6359pxkr49bc2bikg";
    feedsSha256.packages = "0yr94bcml2zy856wd1w7c4yvb2159x3slrfq2qldpai4w7552xld";
    feedsSha256.routing = "16h642wndffbkhhbc62p1jjw37wzbylb455f659kj7qsxh6wrm0b";
    feedsSha256.telephony = "1hamds3bzw87c3iiap3j38h41rcaz0qk0sc6902y3bqlrq60scys";
  };
  "bcm47xx"."generic" = {
    sha256 = "0n8bcrd81sj7rkqibzf7x9cjci155im284srqnnam2qa1g3iajqh";
    feedsSha256.base = "1hai36lv4g7pc0hsycapyy0khby010y1dm9hk4phjk5yy188bkyh";
    feedsSha256.packages = "104ah58g4x9xf5lkm4k165xkjncbxhkd22gc0c1bmblq8r3i8nz8";
    feedsSha256.routing = "18296hac467f8m60ldqrzbadpnd8jb8lslx9pb2zbfg14ck04hx2";
    feedsSha256.telephony = "0pfvmvbhi9ayz79lakaxrv5n0g173khmmknyi4b385pxidg6wfgl";
  };
  "bcm47xx"."legacy" = {
    sha256 = "1zshxxharkvyvhqa3ysaagh5phdg20d15d9rbrs6lh62ba1kny6f";
    feedsSha256.base = "1hai36lv4g7pc0hsycapyy0khby010y1dm9hk4phjk5yy188bkyh";
    feedsSha256.packages = "104ah58g4x9xf5lkm4k165xkjncbxhkd22gc0c1bmblq8r3i8nz8";
    feedsSha256.routing = "18296hac467f8m60ldqrzbadpnd8jb8lslx9pb2zbfg14ck04hx2";
    feedsSha256.telephony = "0pfvmvbhi9ayz79lakaxrv5n0g173khmmknyi4b385pxidg6wfgl";
  };
  "bcm47xx"."mips74k" = {
    sha256 = "0q0q89c86lk1758sb7zm42cc3gy8d9rs2c6my5qmkxpdf4l8fbfg";
    feedsSha256.base = "1h5brrrcvy607kcq2434dbcp0r6n1d61wm478cghkm78yn73ddzn";
    feedsSha256.packages = "1nlfzjhvd93s3rz96vxjrrk1q0g6w2l35a8zmf5bdwsza3dazv9b";
    feedsSha256.routing = "096xgv1f7s6zk8nc9dd1rx2zhj369m24p3jjvkbnvqhp2wgvml5k";
    feedsSha256.telephony = "0ihjb4mzd64qalplfp24dqfwykk3v92j2mggjsd1xbqd54fmw3p0";
  };
  "bcm27xx"."bcm2711" = {
    sha256 = "0lfd54gf4qkxwyz4mvrpkffx3lh8bycrs8b0q85d1ggsglv470sc";
    feedsSha256.base = "1ag19ymm5xkil3cdxp7bmsg45qfgsk0b5symjl2ymrmsgx673q9p";
    feedsSha256.packages = "062s8p3b61il1cklz8dgzxjzld6fmka1i3c7y2br2h28vm95yy60";
    feedsSha256.routing = "0kvcwd2hpmxj6bl8bk4az6rlmxww0g8dhfj3ar7sf9si8hganmqn";
    feedsSha256.telephony = "0kmkwdnv4ldkpakws6w574f28nw6nifvpx76bzcrpd7ysi72fa6f";
  };
  "bcm27xx"."bcm2708" = {
    sha256 = "1ilqpy5dxqw1nrq5a6qnn1szhzklzil9fz8wxy6v5mfallh8jg69";
    feedsSha256.base = "1gwcikhyc640ag70064lb1fpl6jlidxpazcdc7fagrklllcjips5";
    feedsSha256.packages = "1vlhwgrh65s9vm47b4g8jik0adw5jj8840qngpyz1n2rdxpyavg3";
    feedsSha256.routing = "11rs6wblyy2ch3g8n2paas0z7n35ff2wyp5nqrnqvxm4nyxlfqrv";
    feedsSha256.telephony = "187s2gliry9jv4va1sqfvwdrl20mhzb424k10747pfraa3b6dc8f";
  };
  "bcm27xx"."bcm2709" = {
    sha256 = "0bvhz8g50ma71z6xprbfm06nrfk5909mpggz274ivn9mx1sr837l";
    feedsSha256.base = "1ixc0p3x7w2sbzaihakr7dx8353klb8nl9kgdis3gdp7wlmizns5";
    feedsSha256.packages = "110f6wlfx5fmyh3lzqxfc4iyd638v63d3sv863wqrh9fr3hvrvxs";
    feedsSha256.routing = "1ykxb09mxljln34xdhk63n63d4a43cyfc9wf020mp2d0p81afcb8";
    feedsSha256.telephony = "1m8f5p1j2xcxq9f3xdi6g1pgxc4shk3wm5wypmkas3z69wjz609q";
  };
  "bcm27xx"."bcm2710" = {
    sha256 = "170i5dkqb8jv7gvsarbly357hj7g29l6h5m01bvlzpdx9f7xklzg";
    feedsSha256.base = "0lqycbml2957rsm5vy2ny3mcrb8rwnm5vfs35mmfmnanxx3z6p5w";
    feedsSha256.packages = "1m0vkybln9qrbzii9z2yl83fpbc42n6if000bzvmrp3x9yjqb0jp";
    feedsSha256.routing = "1rbzz3n919z7kvyl50s6c2mbfbrki6m4s5z2y5jh07czyh2vyarc";
    feedsSha256.telephony = "1m8vy9s7g8xfq73h23wdsmxz9h68q0yr1aa2c5aqfzdvd4i0hdhw";
  };
  "mvebu"."cortexa53" = {
    sha256 = "19i6n9qxn588hfj25c2yniivvgppk9dzls32hqk3fdfnkhkscsjv";
    feedsSha256.base = "0lqycbml2957rsm5vy2ny3mcrb8rwnm5vfs35mmfmnanxx3z6p5w";
    feedsSha256.packages = "1m0vkybln9qrbzii9z2yl83fpbc42n6if000bzvmrp3x9yjqb0jp";
    feedsSha256.routing = "1rbzz3n919z7kvyl50s6c2mbfbrki6m4s5z2y5jh07czyh2vyarc";
    feedsSha256.telephony = "1m8vy9s7g8xfq73h23wdsmxz9h68q0yr1aa2c5aqfzdvd4i0hdhw";
  };
  "mvebu"."cortexa72" = {
    sha256 = "11njqhhyqcz7v011awslpx8lnfwjnfrgsbb4phd745w2v87f9fza";
    feedsSha256.base = "1ag19ymm5xkil3cdxp7bmsg45qfgsk0b5symjl2ymrmsgx673q9p";
    feedsSha256.packages = "062s8p3b61il1cklz8dgzxjzld6fmka1i3c7y2br2h28vm95yy60";
    feedsSha256.routing = "0kvcwd2hpmxj6bl8bk4az6rlmxww0g8dhfj3ar7sf9si8hganmqn";
    feedsSha256.telephony = "0kmkwdnv4ldkpakws6w574f28nw6nifvpx76bzcrpd7ysi72fa6f";
  };
  "mvebu"."cortexa9" = {
    sha256 = "1993n7csdb241069v6gvw2sh36mnnvspqj7wjj8qvczhn4gclr2b";
    feedsSha256.base = "0kl7a0j1glkajmand8kyr6yfcs5d46grh6cgkmwik7hkm594p640";
    feedsSha256.packages = "1bnbim8y1f2saa3xln2m0lvn1r6mi2jwjbvx1d0hl0jrgzv9bk1h";
    feedsSha256.routing = "1lf4hnjfxgg598ax442c3gf7611x5fdxc3a54slz3a2wny3pdsf4";
    feedsSha256.telephony = "0a3c25akfqdy7mb7d9qziqlpr73yx94wdsprqz2hg03k8j8xcahb";
  };
  "at91"."sam9x" = {
    sha256 = "1njyimh7574pf2pxwwqma9x3ysjzcipxdrki22hzkp9pd35863j2";
    feedsSha256.base = "0pm47qp2qjgc8y01n0kipj1zck3isywh2ha4yhfrhlc8f8df4yrx";
    feedsSha256.packages = "0rqcwccxp3bkgyrgb2iz0x73niv4rjc61nzy98wlz218cv678a0i";
    feedsSha256.routing = "0rhf4ym830sbwphdd404bzzfqbbmkzq53ws10f6j9px2s005wi2m";
    feedsSha256.telephony = "0sgpy3x0z19zlb2gw1ffrcpdb89ma5i7l2b4rscird3b01z7b5k6";
  };
  "at91"."sama7" = {
    sha256 = "11ckqidqwf0r4bmq7dnypffmaxyl42bx1ff8246zhd6fw9cylydq";
    feedsSha256.base = "089i01mbcx3lgfvppyqgp0wph6vd0dhrpzs12cym5hgrznfbyyna";
    feedsSha256.packages = "0mywqrzykgwp18ayfrjlmyn3k3md1dmaf1lskmh7v8lcypd3pza7";
    feedsSha256.routing = "0qxcnmy6hci3dirshy38na10pw1hd27w4jxxp58hli6ixy8lssnr";
    feedsSha256.telephony = "1xi0c5a63gc2z5hk54wzraa40fir55xai6p0w5y9knigh4p1866f";
  };
  "at91"."sama5" = {
    sha256 = "184plf226qac83abnhvj5ibcbp97l2ly44vfwxlrfkwmbxl4g34b";
    feedsSha256.base = "0k4kjci7z5b6sfkchqhlsdxk04g47qg8xmpgaj0kx5mg8mzvjqnm";
    feedsSha256.packages = "0fy111nw33zmzhnhz2wxqia12sb1mys085jlqpmpfa1k44s1npkm";
    feedsSha256.routing = "1x3m44zd0yn7faaj5p8drpc7nnlb7y3ybs45010x8ll7vqd3i8w0";
    feedsSha256.telephony = "1qa8isxpm5gla4kvrmzp2pd70493x0a8z9akcxwj9yg8i71qksn2";
  };
  "gemini"."generic" = {
    sha256 = "07sfzsblhwb8bgw2dh61mbiwi5ii8gabd4gd8448ygkpxczqvkqy";
    feedsSha256.base = "12i08c8vajni3zvw5z8iyxlavkdzh50h652lald45jxp66sfn201";
    feedsSha256.packages = "0lyjgip2kmji1c3mw7hil99wy74cg1md99xc7p1ym8ipn6l65c78";
    feedsSha256.routing = "1apnkiip76mdbf1w1n8a62dqxyckz69h5j6w07dd38gshy3pznbq";
    feedsSha256.telephony = "17xfncribrfs0c5dzylgy1aywnkwljnnjyaf22gqgzj5w72d849b";
  };
  "octeontx"."generic" = {
    sha256 = "0qkldws9vbv2z3z7d1ymi7nymjncmvcmmynwz3qcl14rd86iqncb";
  };
  "ipq40xx"."generic" = {
    sha256 = "16yidkj6nsz18mbvc87gpdxxp36qsys5rvn1sa17fafmfjf49lsl";
    feedsSha256.base = "1ixc0p3x7w2sbzaihakr7dx8353klb8nl9kgdis3gdp7wlmizns5";
    feedsSha256.packages = "110f6wlfx5fmyh3lzqxfc4iyd638v63d3sv863wqrh9fr3hvrvxs";
    feedsSha256.routing = "1ykxb09mxljln34xdhk63n63d4a43cyfc9wf020mp2d0p81afcb8";
    feedsSha256.telephony = "1m8f5p1j2xcxq9f3xdi6g1pgxc4shk3wm5wypmkas3z69wjz609q";
  };
  "ipq40xx"."mikrotik" = {
    sha256 = "0m1mbwhpy46avhcv9dwg7b1iqh4jcgxyf4j400x7k8j558bq44a9";
    feedsSha256.base = "1ixc0p3x7w2sbzaihakr7dx8353klb8nl9kgdis3gdp7wlmizns5";
    feedsSha256.packages = "110f6wlfx5fmyh3lzqxfc4iyd638v63d3sv863wqrh9fr3hvrvxs";
    feedsSha256.routing = "1ykxb09mxljln34xdhk63n63d4a43cyfc9wf020mp2d0p81afcb8";
    feedsSha256.telephony = "1m8f5p1j2xcxq9f3xdi6g1pgxc4shk3wm5wypmkas3z69wjz609q";
  };
  "mediatek"."mt7623" = {
    sha256 = "00flg9af1wygrwxs50b4w4bb8n5914r53s9nxhq3619lp6hdwbr3";
    feedsSha256.base = "1ixc0p3x7w2sbzaihakr7dx8353klb8nl9kgdis3gdp7wlmizns5";
    feedsSha256.packages = "110f6wlfx5fmyh3lzqxfc4iyd638v63d3sv863wqrh9fr3hvrvxs";
    feedsSha256.routing = "1ykxb09mxljln34xdhk63n63d4a43cyfc9wf020mp2d0p81afcb8";
    feedsSha256.telephony = "1m8f5p1j2xcxq9f3xdi6g1pgxc4shk3wm5wypmkas3z69wjz609q";
  };
  "mediatek"."mt7629" = {
    sha256 = "0l4r4bag24a9if8i18d3y31x6gd2mgcynvm9jbd8k4dsp79sbb5d";
    feedsSha256.base = "0vhk8sf0jks8lwpxycyzzd5px39yll9nzrkxnxp972iinc7i2j6j";
    feedsSha256.packages = "15ghwagmix0i5davnhrqf8j4l4cfz613avj3y1k7b7j1vqsrfpir";
    feedsSha256.routing = "0mnk1xzj58cjqv0x27idc8wcr1f7plqj2hlg2fvjmg4piy7h5wk0";
    feedsSha256.telephony = "0bw1qr2nyp93igz4rf6s0mwykdl5c0qn6sfd2x24hvw93g900567";
  };
  "mediatek"."mt7622" = {
    sha256 = "0i41zli564vh82yi2izyxc888bv47l0zkp72lkncghn1j73jyyhz";
    feedsSha256.base = "0lqycbml2957rsm5vy2ny3mcrb8rwnm5vfs35mmfmnanxx3z6p5w";
    feedsSha256.packages = "1m0vkybln9qrbzii9z2yl83fpbc42n6if000bzvmrp3x9yjqb0jp";
    feedsSha256.routing = "1rbzz3n919z7kvyl50s6c2mbfbrki6m4s5z2y5jh07czyh2vyarc";
    feedsSha256.telephony = "1m8vy9s7g8xfq73h23wdsmxz9h68q0yr1aa2c5aqfzdvd4i0hdhw";
  };
  "malta"."be" = {
    sha256 = "1x4bl8jhy03vpj2azhx4yrp90kp681gzpliv1awiipggim19vvq9";
  };
  "rockchip"."armv8" = {
    sha256 = "1gk249crf3jzvhlaz0hgg4kks9b7fnll6vpps3zjxfk07r0hhzz8";
    feedsSha256.base = "1q3cmvl6sq18v3rn9gpqr7fhwnjnv4lgllklcd8n11rg8rfgnax9";
    feedsSha256.packages = "1bjwr6zsbhrgkr93b4mgcgqlcbvdi4sia71h91cyfsc6zsh2n195";
    feedsSha256.routing = "1dvmly64q68nn751fwxq164jiqpv4ap7xh6km2x11af51dn5h972";
    feedsSha256.telephony = "19s63754bkxdwqmdyanv74b0qw97y0nql8ad92x99f0z8rj310g4";
  };
  "ipq806x"."generic" = {
    sha256 = "0prfsc1qfbs18i9985kz4alm6g7rlqzbwany9njvhi6nsny3cvbs";
    feedsSha256.base = "01snak73ac4dypqf6fsz996dmzqwdixypvldy99v0g5lr1pln8fx";
    feedsSha256.packages = "0ijc05qp7h3ras56bixvqbnnd6gshqkk51lx2avy4s475fs9nmkj";
    feedsSha256.routing = "1832lbmyz4dgziidd8j4li4522004jq349wa6vvmi9ykalf1wfiw";
    feedsSha256.telephony = "1xibrl5pccxwwka672i468n0nd6bgi8580vm4zvmn2fczbyyj1gc";
  };
  "sunxi"."cortexa8" = {
    sha256 = "0yza6a2hgirvwf75n9y8fh9w9kcny4nh85ldsxxl37hnds8f9z4f";
    feedsSha256.base = "0i7bzyyq85k9mbmfx6rx5ajfsppa9g98hjvg4yy1v91qzd5bl2iv";
    feedsSha256.packages = "1vyryifsypx4mag03d3m5pahw969kmjjfzkddjnxa92n6yccxnk4";
    feedsSha256.routing = "0nr5asirky43pdd5312a21820bgzmqw6j4p07jv15fiv10s6n4ha";
    feedsSha256.telephony = "1fznl171s455zk5m9mdcrls9jgxf36yfvqp4xlbvsfn5m9axgha6";
  };
  "sunxi"."cortexa53" = {
    sha256 = "1h9m4hgrdhws1nm8wzh34c4vzr1a2fn0qp9kkrcjf240ads48l7r";
    feedsSha256.base = "0lqycbml2957rsm5vy2ny3mcrb8rwnm5vfs35mmfmnanxx3z6p5w";
    feedsSha256.packages = "1m0vkybln9qrbzii9z2yl83fpbc42n6if000bzvmrp3x9yjqb0jp";
    feedsSha256.routing = "1rbzz3n919z7kvyl50s6c2mbfbrki6m4s5z2y5jh07czyh2vyarc";
    feedsSha256.telephony = "1m8vy9s7g8xfq73h23wdsmxz9h68q0yr1aa2c5aqfzdvd4i0hdhw";
  };
  "sunxi"."cortexa7" = {
    sha256 = "1k00v1blrigyc72g09396nr6cywkkd3kzg63b5hyjakzr5is3ip1";
    feedsSha256.base = "1ixc0p3x7w2sbzaihakr7dx8353klb8nl9kgdis3gdp7wlmizns5";
    feedsSha256.packages = "110f6wlfx5fmyh3lzqxfc4iyd638v63d3sv863wqrh9fr3hvrvxs";
    feedsSha256.routing = "1ykxb09mxljln34xdhk63n63d4a43cyfc9wf020mp2d0p81afcb8";
    feedsSha256.telephony = "1m8f5p1j2xcxq9f3xdi6g1pgxc4shk3wm5wypmkas3z69wjz609q";
  };
  "mpc85xx"."p1010" = {
    sha256 = "0g060f60xb8b3byxyrg9q4m09rlzfbdkxlrp687r6a901n1m55ks";
    feedsSha256.base = "04id7kx2xjwv0lvf2dq75l208s1m0953011ch8p42d4srn5rwv31";
    feedsSha256.packages = "0hwzxkb3ys32lpvjmywn1g7yn3668fq7qq978qjrsqsf9sq4cg8c";
    feedsSha256.routing = "0cnmikimk16is6g9y00397xq680k8hwb723q320nnv323inqs8mh";
    feedsSha256.telephony = "07l4hnbnkagjp5ldz42p499svl2xg64n6pznb2xagxnkycd5dd6p";
  };
  "mpc85xx"."p2020" = {
    sha256 = "0558fh18s25nsj8hm31hjpr2mim5db0qknywvifmi2kyz6sa91am";
    feedsSha256.base = "04id7kx2xjwv0lvf2dq75l208s1m0953011ch8p42d4srn5rwv31";
    feedsSha256.packages = "0hwzxkb3ys32lpvjmywn1g7yn3668fq7qq978qjrsqsf9sq4cg8c";
    feedsSha256.routing = "0cnmikimk16is6g9y00397xq680k8hwb723q320nnv323inqs8mh";
    feedsSha256.telephony = "07l4hnbnkagjp5ldz42p499svl2xg64n6pznb2xagxnkycd5dd6p";
  };
  "mpc85xx"."p1020" = {
    sha256 = "1v2y0rarv4yfnsl6dg6ki0gbybk82sv63in3ws0fqck9crng6i66";
    feedsSha256.base = "04id7kx2xjwv0lvf2dq75l208s1m0953011ch8p42d4srn5rwv31";
    feedsSha256.packages = "0hwzxkb3ys32lpvjmywn1g7yn3668fq7qq978qjrsqsf9sq4cg8c";
    feedsSha256.routing = "0cnmikimk16is6g9y00397xq680k8hwb723q320nnv323inqs8mh";
    feedsSha256.telephony = "07l4hnbnkagjp5ldz42p499svl2xg64n6pznb2xagxnkycd5dd6p";
  };
  "imx"."cortexa7" = {
    sha256 = "0a0rvswvkqd7gijkiv0vch9vma483w4xjnw6h3xh618yi9xslxbp";
  };
  "imx"."cortexa9" = {
    sha256 = "0n9hxh9wl8qaszdcsl7vn6vjf5631d4qy4f4gs0vxl3biqvjp171";
    feedsSha256.base = "18j34bcl0wwbg3kbbv5bpf6k3kcj31rwcpqljgqz09sbg6p4fm53";
    feedsSha256.packages = "0a7n5rnvv2y075v7h085sq6r65i0mzlnz9f2xch429qq2yrv04fs";
    feedsSha256.routing = "10gzaa5jvlpr4fbqhah8h2ag0ybma0fby54yyd115fx782s3pmv6";
    feedsSha256.telephony = "12v8w8ba85s53gh88mb66m7jyvk1dvmvvhk79n79wplpwp1armsk";
  };
  "x86"."generic" = {
    sha256 = "0hfqv9zjvx86vjdmzgipwhrzc0asmkpvc6g4b3wagxaqr1y8249y";
    feedsSha256.base = "0p5dgqhmplxsmaly55b50w23mmyxhbz5v74r1f1sv6qqzq58aj9k";
    feedsSha256.packages = "0k0qzw7rysfh9z0dcwjki4q22qwpp50gpxipwwf26lagmx6d2qfv";
    feedsSha256.routing = "0mm3j3q0dfhl6lap9l7k3njnsm22ggvdag3ghrdvvppadk8ici4k";
    feedsSha256.telephony = "1b2glkqwsrddq06jiq0nnprlbzgi6wh5vyp4zmmpdcif3c5qny49";
  };
  "x86"."legacy" = {
    sha256 = "1l57fxgp3rwws9fr8013lkpa4lr314vr1271i0vnriwcxa2hcwqi";
    feedsSha256.base = "1511a9y3nwdwkvgf0qjnw7l6m27jffbaxjcx8szppby96x8bwxly";
    feedsSha256.packages = "0zbp24szygp372zd7i92d6xi2a520ks081i50sbbdmmd264kp5dy";
    feedsSha256.routing = "0v61679xbpzs7vj4mnxya6672kp18vnmvc0zhvkms8qpxh188gsv";
    feedsSha256.telephony = "1i17q192wszv30rq5yfnr39wmdf0qbyh4yxq7k2b098mrzxnn40n";
  };
  "x86"."geode" = {
    sha256 = "0wri8v0yxllyyv4aw96ybd7c88yp5jz4vfir6ilg18jxb8ssq6x5";
    feedsSha256.base = "1511a9y3nwdwkvgf0qjnw7l6m27jffbaxjcx8szppby96x8bwxly";
    feedsSha256.packages = "0zbp24szygp372zd7i92d6xi2a520ks081i50sbbdmmd264kp5dy";
    feedsSha256.routing = "0v61679xbpzs7vj4mnxya6672kp18vnmvc0zhvkms8qpxh188gsv";
    feedsSha256.telephony = "1i17q192wszv30rq5yfnr39wmdf0qbyh4yxq7k2b098mrzxnn40n";
  };
  "x86"."64" = {
    sha256 = "1s2j491hv78kfscdq3zpy002pgam2ag0qsp6d6md6451qdlkncab";
    feedsSha256.base = "0q986sazkyfv43zyqbf8wbyplzc8hiqy0pvkhvmpbym73lvnksvd";
    feedsSha256.packages = "0ank3ig7hnn006ibsw1491lw8z6yy91f8lkc4xymsss1shfks4rs";
    feedsSha256.routing = "0q1dmsllmcpsrjsbgskwlcihfkmjxvlmb2gbyc3vzh35wlv9mv4j";
    feedsSha256.telephony = "1fagyv7b9603cajpwppk4p1irjq3lj7yrr2bmyqbb02w8ili2mh5";
  };
  "realtek"."rtl838x" = {
    sha256 = "0z2wqdpj06dfj8cmrp5adbarj2mwcrlvadscv3brsmzy1ds00j54";
    feedsSha256.base = "00pb5fr6m6wagsz7qf23rd6lmi041avaqh9954s4kg8wd5gcqdia";
    feedsSha256.packages = "1vf37k49jhxb0b5dnznjzadcqggm6ms6g6pxb4p92g5yjvz07iv6";
    feedsSha256.routing = "1a12bgq4lkn2jm9hx5w1b2m0ym2rp2jyq34khh5rl76dcsb78a79";
    feedsSha256.telephony = "1rzbrawg9janl5a5q16mnzyrakim0500ydnwbggpv9pnl6my80h5";
  };
  "realtek"."rtl930x" = {
    sha256 = "1pvw2hphjck7ypif9x6swzcxkgy5l017l7bjrpgicdhfj5ahi5n8";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "realtek"."rtl931x" = {
    sha256 = "019x3bys6ixd017j3qmdbc5yrpvirzihpc7mh5pc4fg4wwpqalkn";
  };
  "realtek"."rtl839x" = {
    sha256 = "02yc2lq0g8zjzpbh9zqsbhw8pb368gxnzws5qpxyqbc5ffjjmpyh";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "armvirt"."32" = {
    sha256 = "1zdkxafg1nq8pkfr2fs85mwkgs83aih3yiwkmm4byiw0fh03q1n8";
  };
  "armvirt"."64" = {
    sha256 = "172rcr7syhzrzy5jkyaqsfz6v6djfczfv0s70i94anq1mz98v3ma";
  };
  "kirkwood"."generic" = {
    sha256 = "1ni7p5y195sh85m2b4y0m2wgds4cypfgk00gsk5ijp0k5d6yw01j";
    feedsSha256.base = "0vpijc1v3mli29m1cs987d5x0rcfng03yw5s65djfmmkclhvvg0z";
    feedsSha256.packages = "158z98g5l6k6fr3lzjqgr7700vrf2qnjjfwk704qhbh5h2xl18x7";
    feedsSha256.routing = "0ln9xd2dapsj3ckzfiy0qs68rar242k1a5xvk0858cw5w25dngx1";
    feedsSha256.telephony = "1mza6d1bajgf12gcs4z2d2dgc0akq9146gqpwmp6n17d9grqxszx";
  };
  "ath79"."generic" = {
    sha256 = "140i1y91rmcf0dpp23fbj863p2szsj98pik3zpvr44iw4wd0wbl5";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "ath79"."mikrotik" = {
    sha256 = "05wh44irvng5xpqqppz82n268lgvlv6vi5b3mxb70cpsx3d2kr05";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "ath79"."nand" = {
    sha256 = "0dk5lcyvcsb3ylw5i1wvhn5fdngvvqb0jl3bww5c2whq4xxv1l2k";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "ath79"."tiny" = {
    sha256 = "19bw73adrjg2yccj0sjc075nz85l90hkg2kzmdqa17p388djfxqw";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "archs38"."generic" = {
    sha256 = "1hqyvwnkrswmnciczzjiij3ama9ihd2f0g8m6cw8kwn4cy7cxvgq";
    feedsSha256.base = "0f4cji5m4j82yvr9x677agyrdmwsmif2iwb11h39rm9sfw669abf";
    feedsSha256.packages = "07ws8g2g94pf6lwdxxcdgyqdsm17h49ycdla7y9wgxw5blbmbw6l";
    feedsSha256.routing = "0227cwa2a2gqyhhij3xrx3sx9528mx07dqs2vsp77z91w0i88bpk";
    feedsSha256.telephony = "00a7y5ld5al6k8h89gjxnarqsix3p28pcn8c6n3g543w3ndx5d2r";
  };
  "ath25"."generic" = {
    sha256 = "0hh7h7plfwfr9cy2g0hbr10zsxvz3fg8fd6a22x3ir94qzjpacyc";
    feedsSha256.base = "1ym8chhsjn9qyms5bncbbayiy3gjr2xsv4a6359pxkr49bc2bikg";
    feedsSha256.packages = "0yr94bcml2zy856wd1w7c4yvb2159x3slrfq2qldpai4w7552xld";
    feedsSha256.routing = "16h642wndffbkhhbc62p1jjw37wzbylb455f659kj7qsxh6wrm0b";
    feedsSha256.telephony = "1hamds3bzw87c3iiap3j38h41rcaz0qk0sc6902y3bqlrq60scys";
  };
  "apm821xx"."nand" = {
    sha256 = "03vippx6fzpnpl4pd4hr589h0vhsyi2dy7p2q6y3j9rz89kfcbs9";
    feedsSha256.base = "0slr0bjry89m556ijdndlhh5rh8qmgn7fr7fqi8qi9n6551y8wk1";
    feedsSha256.packages = "00dvnaim0rjyahhs0k15rbywpc1wrazdmph9s2fk353pjjhl81i0";
    feedsSha256.routing = "1imshxcz0hq9kvdv278ziyh0wjhnaygr7pgrgzalb02w8ysfhkl1";
    feedsSha256.telephony = "03573h00zs4wj2qhdwvwjksn79w4qiqadlgdb02dxq5krbr4gp92";
  };
  "apm821xx"."sata" = {
    sha256 = "0w2qz0pnnmcz3rdgpfanvsl099wmsfsfqda42pmbjq256379lr7g";
    feedsSha256.base = "0slr0bjry89m556ijdndlhh5rh8qmgn7fr7fqi8qi9n6551y8wk1";
    feedsSha256.packages = "00dvnaim0rjyahhs0k15rbywpc1wrazdmph9s2fk353pjjhl81i0";
    feedsSha256.routing = "1imshxcz0hq9kvdv278ziyh0wjhnaygr7pgrgzalb02w8ysfhkl1";
    feedsSha256.telephony = "03573h00zs4wj2qhdwvwjksn79w4qiqadlgdb02dxq5krbr4gp92";
  };
  "bcm53xx"."generic" = {
    sha256 = "1vqwr4p0xgvj0iplwhd9csnw88nqhmw655kr9djalk0v61mgh5gb";
    feedsSha256.base = "1isg73zz5h7ays37q74icvgp62i4fm739lllc9p1mz1f3whr5fcf";
    feedsSha256.packages = "1hab1wxb7zdgkr46zv5ggg0cnq52yvzv78wvg3a5if8sklvd1x1d";
    feedsSha256.routing = "185rfd9bkhmaf5v6kxn3ppr52dr46rwmpdm8gcwli4xhk28qy6jh";
    feedsSha256.telephony = "15v307nxnsg20dgshmxydlx76zig7cv02pf7b9s4rxjsmyl5bm52";
  };
  "ramips"."mt76x8" = {
    sha256 = "15iskbpmqfxgfzjmxj7r3ha78nb18i4dh0s0axm7322lh7qac7f0";
    feedsSha256.base = "0jpk429ppqmk0pwlxgcw8nnr0phfbfxwzlgdwyvsws8fqxsbrp02";
    feedsSha256.packages = "0npj5ngpmr1illlwxz9n9gqxmb9n37smggr9l2jr9p39cfgjsr5b";
    feedsSha256.routing = "0rj4if4vd6id84s745mv29mz0wh4acx5fplz1kcwqvf456zwnb7h";
    feedsSha256.telephony = "0f57d18bllbmknh598jiw7bm8lc2kfkiby0xpgjcp1w8j7nxdnnr";
  };
  "ramips"."mt7620" = {
    sha256 = "0v6phqrl96chnfhzvrajay46hjvmrypnf7nmqg8imlna1kssmckh";
    feedsSha256.base = "0jpk429ppqmk0pwlxgcw8nnr0phfbfxwzlgdwyvsws8fqxsbrp02";
    feedsSha256.packages = "0npj5ngpmr1illlwxz9n9gqxmb9n37smggr9l2jr9p39cfgjsr5b";
    feedsSha256.routing = "0rj4if4vd6id84s745mv29mz0wh4acx5fplz1kcwqvf456zwnb7h";
    feedsSha256.telephony = "0f57d18bllbmknh598jiw7bm8lc2kfkiby0xpgjcp1w8j7nxdnnr";
  };
  "ramips"."rt3883" = {
    sha256 = "1ppyfv5h0lvl98vg062gncfqzr6y2xslxz0nnd3av0bisbsj1gyv";
    feedsSha256.base = "1h5brrrcvy607kcq2434dbcp0r6n1d61wm478cghkm78yn73ddzn";
    feedsSha256.packages = "1nlfzjhvd93s3rz96vxjrrk1q0g6w2l35a8zmf5bdwsza3dazv9b";
    feedsSha256.routing = "096xgv1f7s6zk8nc9dd1rx2zhj369m24p3jjvkbnvqhp2wgvml5k";
    feedsSha256.telephony = "0ihjb4mzd64qalplfp24dqfwykk3v92j2mggjsd1xbqd54fmw3p0";
  };
  "ramips"."rt305x" = {
    sha256 = "1miklm05q82wr3c002a48mp4l4yfn42hinfiaw120qqkr6hp2vkw";
    feedsSha256.base = "0jpk429ppqmk0pwlxgcw8nnr0phfbfxwzlgdwyvsws8fqxsbrp02";
    feedsSha256.packages = "0npj5ngpmr1illlwxz9n9gqxmb9n37smggr9l2jr9p39cfgjsr5b";
    feedsSha256.routing = "0rj4if4vd6id84s745mv29mz0wh4acx5fplz1kcwqvf456zwnb7h";
    feedsSha256.telephony = "0f57d18bllbmknh598jiw7bm8lc2kfkiby0xpgjcp1w8j7nxdnnr";
  };
  "ramips"."mt7621" = {
    sha256 = "0ydhaq7x4wxh8siv8szhl996x52ky77x8rgcm9z2s9gy5q5ymhkh";
    feedsSha256.base = "0jpk429ppqmk0pwlxgcw8nnr0phfbfxwzlgdwyvsws8fqxsbrp02";
    feedsSha256.packages = "0npj5ngpmr1illlwxz9n9gqxmb9n37smggr9l2jr9p39cfgjsr5b";
    feedsSha256.routing = "0rj4if4vd6id84s745mv29mz0wh4acx5fplz1kcwqvf456zwnb7h";
    feedsSha256.telephony = "0f57d18bllbmknh598jiw7bm8lc2kfkiby0xpgjcp1w8j7nxdnnr";
  };
  "ramips"."rt288x" = {
    sha256 = "0clp1b7v86bw1cgqg9h87vfrsgdpa5rhp81vhz84rw0ccb762pph";
    feedsSha256.base = "0jpk429ppqmk0pwlxgcw8nnr0phfbfxwzlgdwyvsws8fqxsbrp02";
    feedsSha256.packages = "0npj5ngpmr1illlwxz9n9gqxmb9n37smggr9l2jr9p39cfgjsr5b";
    feedsSha256.routing = "0rj4if4vd6id84s745mv29mz0wh4acx5fplz1kcwqvf456zwnb7h";
    feedsSha256.telephony = "0f57d18bllbmknh598jiw7bm8lc2kfkiby0xpgjcp1w8j7nxdnnr";
  };
  "lantiq"."xrx200" = {
    sha256 = "0nh164jxxh1izpw6yiyly1vcba0fa1g7ag9nvc0chj5xf3pb4dm8";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "lantiq"."ase" = {
    sha256 = "10mimnl22hcvr5arkprbfnlwpzgccp71z50cpwnnf6rsmccrh65p";
  };
  "lantiq"."xway" = {
    sha256 = "03alzy71iywj94wp0cm007qnhzj1r3rjdx232vsag4xhn6hfnnkm";
    feedsSha256.base = "03w8shlaikf7611w487cphffvpmfh71vbhia5d3ww7hx8ffgi46a";
    feedsSha256.packages = "1m3j7kijznj03xcjv06cf5xi5al9rj4s5xvg4k8xfkdwnfn4gkmm";
    feedsSha256.routing = "1r8pxcdf2kg3n6dwxfrbrblhwjravzn36qv9p9l38x94z61is3wi";
    feedsSha256.telephony = "0hnz6avr9kbzabc5dhskldg6mq808s92xbw1dr3xz57vbw1s77w9";
  };
  "lantiq"."xway_legacy" = {
    sha256 = "0a8adzv0053ymyvyxksnp63bw2b1qh9idzcx224cdjsxxsr10pd4";
  };
  "octeon"."generic" = {
    sha256 = "00ivwxzna08s5a1823yh5ag0xbq2kjfzpc5pkgm18qbqjw29mb73";
    feedsSha256.base = "0d6b3zjbbl9jsaddpymd5301h0hw6dwrdalcg5vq3w0pn7kic0yy";
    feedsSha256.packages = "1qvkx645sf4njdljhj1fgqccndhqxz7kpcvsw3fzyg06582njwrd";
    feedsSha256.routing = "171bjb5v8a2cwf57jwxdn0zqzmqpgxqwvb3qq7hwqcqivw9dwpxf";
    feedsSha256.telephony = "09cyvpdds90y0yshh2cd7g1apjlh48w7xf7gm5gzbflyavsxzn42";
  };
  "pistachio"."generic" = {
    sha256 = "13rghzks9yivjbvk234ybv3yrdmm3lmqrv6jl98apznkbk0b9mag";
    feedsSha256.base = "02ncfqr2w4dp0s4ypk65bpwyli7xj74mmlr5xxik1i58xgihqbjq";
    feedsSha256.packages = "0b4jvcf5nfkrz05syabb40gcgpbqjl4zchz8k97x0ixyq36df8sk";
    feedsSha256.routing = "0w1z5dzw8rg0s2v0ha4phl0bhzbpn6sa7varhkdhxj7i6f2qafwz";
    feedsSha256.telephony = "1pzx4j0b6si9ix1l75vnzh7w8dsivhw20d81b7ad3wlyg3jlqmp0";
  };
  "layerscape"."armv8_64b" = {
    sha256 = "1kzpn0vq6yaixg9zdvy17wf2vycq0777js5vdskx5ym6yd3irxdl";
    feedsSha256.base = "1q3cmvl6sq18v3rn9gpqr7fhwnjnv4lgllklcd8n11rg8rfgnax9";
    feedsSha256.packages = "1bjwr6zsbhrgkr93b4mgcgqlcbvdi4sia71h91cyfsc6zsh2n195";
    feedsSha256.routing = "1dvmly64q68nn751fwxq164jiqpv4ap7xh6km2x11af51dn5h972";
    feedsSha256.telephony = "19s63754bkxdwqmdyanv74b0qw97y0nql8ad92x99f0z8rj310g4";
  };
  "layerscape"."armv7" = {
    sha256 = "0wvwyyamfp2nzic0h3zlr6py32l0by725rv6v3g98dm494qp2411";
    feedsSha256.base = "1ixc0p3x7w2sbzaihakr7dx8353klb8nl9kgdis3gdp7wlmizns5";
    feedsSha256.packages = "110f6wlfx5fmyh3lzqxfc4iyd638v63d3sv863wqrh9fr3hvrvxs";
    feedsSha256.routing = "1ykxb09mxljln34xdhk63n63d4a43cyfc9wf020mp2d0p81afcb8";
    feedsSha256.telephony = "1m8f5p1j2xcxq9f3xdi6g1pgxc4shk3wm5wypmkas3z69wjz609q";
  };
  "omap"."generic" = {
    sha256 = "1cvx9j7qkyqw91c07wyj1w2dq1983hd08m8528a59331shlqq276";
    feedsSha256.base = "0i7bzyyq85k9mbmfx6rx5ajfsppa9g98hjvg4yy1v91qzd5bl2iv";
    feedsSha256.packages = "1vyryifsypx4mag03d3m5pahw969kmjjfzkddjnxa92n6yccxnk4";
    feedsSha256.routing = "0nr5asirky43pdd5312a21820bgzmqw6j4p07jv15fiv10s6n4ha";
    feedsSha256.telephony = "1fznl171s455zk5m9mdcrls9jgxf36yfvqp4xlbvsfn5m9axgha6";
  };
  "tegra"."generic" = {
    sha256 = "13n3jcgwg253nj0h1m7m0bcybpx0lb4yh84acs34s9v09rmwfl99";
    feedsSha256.base = "0kl7a0j1glkajmand8kyr6yfcs5d46grh6cgkmwik7hkm594p640";
    feedsSha256.packages = "1bnbim8y1f2saa3xln2m0lvn1r6mi2jwjbvx1d0hl0jrgzv9bk1h";
    feedsSha256.routing = "1lf4hnjfxgg598ax442c3gf7611x5fdxc3a54slz3a2wny3pdsf4";
    feedsSha256.telephony = "0a3c25akfqdy7mb7d9qziqlpr73yx94wdsprqz2hg03k8j8xcahb";
  };
  "bcm4908"."generic" = {
    sha256 = "004455w93p3y3jggfcqhbz1p8b1hzlasbcw77ijkhklgnbvvasak";
    feedsSha256.base = "0lqycbml2957rsm5vy2ny3mcrb8rwnm5vfs35mmfmnanxx3z6p5w";
    feedsSha256.packages = "1m0vkybln9qrbzii9z2yl83fpbc42n6if000bzvmrp3x9yjqb0jp";
    feedsSha256.routing = "1rbzz3n919z7kvyl50s6c2mbfbrki6m4s5z2y5jh07czyh2vyarc";
    feedsSha256.telephony = "1m8vy9s7g8xfq73h23wdsmxz9h68q0yr1aa2c5aqfzdvd4i0hdhw";
  };
}
