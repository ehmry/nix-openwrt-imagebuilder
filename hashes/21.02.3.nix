{
  "arc770"."generic" = {
    sha256 = "0ll3b59hvxc8q1i589dcnl1r4m5d8qxmwarg75kfsr2rcfv7l7n0";
    feedsSha256.base = "091yr2a880kwisr0hw19174b1lyb26fhdw5zp7p2pq93h8mwgvrp";
    feedsSha256.packages = "0489574h337kkkxinhh2q3var2ypzpghkdsqlq2y4vqdlw7f5jpf";
    feedsSha256.routing = "1ngqrf9balxjcz6x7ilp1fvsm2yl8gpwgn12h1awn54hvpdn9bny";
    feedsSha256.telephony = "06xbcwz3ikpxdfda7fi81jcvx68bp33rj7nh2a5740wlkffvlbg9";
  };
  "imx6"."generic" = {
    sha256 = "1yl8550svchvk8m9x53biq81ya263fznbh4434hylphvnrn7ccsk";
    feedsSha256.base = "1varv9s4b0yl8al8qrqd428bwcahnkr1xgz05j51bkaf1sbc9698";
    feedsSha256.packages = "009b8irb7yp4vgbywbpinrwwh0iqkkmlz97p45hqifdhwcwzk7b9";
    feedsSha256.routing = "186vcyqjy17la4zvza2r0q8mhy43n2h732hc9w2a07jx94mzs5ny";
    feedsSha256.telephony = "07irci03szg02cpl832xq4wl0g1qw43d44pfczwranilymh6xmml";
  };
  "oxnas"."ox820" = {
    sha256 = "0y3v5sfhfmnrxd0hl9rq2778yri102qz0nvpwc2gl0axcw2kh2l7";
    feedsSha256.base = "0bsk01yf0i5pb8j7vmzssgxc5plcc5h1x6wy8mjy3wx2dwayddnd";
    feedsSha256.packages = "04m9mnmqlkmfps9q80kdmgdvqyfhh0vb728mchlx0hp1l7db6nk2";
    feedsSha256.routing = "02dppp5r67klp3s1dnkg26bqmh0wn8cd93prx0q1f782lvw0qsy8";
    feedsSha256.telephony = "0hr4xs2m742a1sps2rv5dx22qisb3n2lwinh7j4w77l2nmj9v8y4";
  };
  "mxs"."generic" = {
    sha256 = "09sb20z8rcs9nisnqd857yw5ddrn0xg26gqinnlg97x9cp8kfzv5";
  };
  "zynq"."generic" = {
    sha256 = "0k1g0v0k76m8042i44s1ifvzi67h92a8czmscvcpb6aj6r2w2iiy";
    feedsSha256.base = "1varv9s4b0yl8al8qrqd428bwcahnkr1xgz05j51bkaf1sbc9698";
    feedsSha256.packages = "009b8irb7yp4vgbywbpinrwwh0iqkkmlz97p45hqifdhwcwzk7b9";
    feedsSha256.routing = "186vcyqjy17la4zvza2r0q8mhy43n2h732hc9w2a07jx94mzs5ny";
    feedsSha256.telephony = "07irci03szg02cpl832xq4wl0g1qw43d44pfczwranilymh6xmml";
  };
  "bcm63xx"."generic" = {
    sha256 = "0rwzrxxaw7s4qx6mqq2b3jnywzqvc4hpwbm2fnbsj4mjd9v66vnz";
    feedsSha256.base = "08lwlj32kym9gwfk2h1mg96b9rvpcrwq2cvpkxlj34c9dfz900sj";
    feedsSha256.packages = "0fldjf2abihip3ij22wcwlyrjfxhm1m9my16vb5qlq4msmfnb07i";
    feedsSha256.routing = "15dnpjk62y540brhxm31rcznka03kwjjkhlbmn337clqz0n84gax";
    feedsSha256.telephony = "1xvdgps6b5icwlppi7fjk4c0jnyy40nzb6mjz47h8abzjbc72fmz";
  };
  "bcm63xx"."smp" = {
    sha256 = "11gz8clwdmn6jgyfj6ysrfza97c2ssql5dlnc319y38cnsciykdq";
    feedsSha256.base = "08lwlj32kym9gwfk2h1mg96b9rvpcrwq2cvpkxlj34c9dfz900sj";
    feedsSha256.packages = "0fldjf2abihip3ij22wcwlyrjfxhm1m9my16vb5qlq4msmfnb07i";
    feedsSha256.routing = "15dnpjk62y540brhxm31rcznka03kwjjkhlbmn337clqz0n84gax";
    feedsSha256.telephony = "1xvdgps6b5icwlppi7fjk4c0jnyy40nzb6mjz47h8abzjbc72fmz";
  };
  "bcm47xx"."generic" = {
    sha256 = "19g1dgfqxmkdlisnsc2kfk2624gzjc0fmwp3zmjz4qmby6y1gk35";
    feedsSha256.base = "09h9fzrb47bgk23vbkwwjnbldmc84v42v7bvk88bcrsl7zzq6ckw";
    feedsSha256.packages = "0xf70vkrwl522fxq6b6s2ny5rzy451vm5i19xqhf2dnp3x99ql0c";
    feedsSha256.routing = "1k455l8kjc3k5y5298b167sr4mj628bffqmqzfs7zn660ir4z7k6";
    feedsSha256.telephony = "06wc1snq9k7miiff88wnqg9gc6wxwbnypksgimw4xi9frcaa8kqi";
  };
  "bcm47xx"."legacy" = {
    sha256 = "12mswd427057011pxzflhl6f8zlh8valmj7wf84bf7fjahj8xd85";
    feedsSha256.base = "09h9fzrb47bgk23vbkwwjnbldmc84v42v7bvk88bcrsl7zzq6ckw";
    feedsSha256.packages = "0xf70vkrwl522fxq6b6s2ny5rzy451vm5i19xqhf2dnp3x99ql0c";
    feedsSha256.routing = "1k455l8kjc3k5y5298b167sr4mj628bffqmqzfs7zn660ir4z7k6";
    feedsSha256.telephony = "06wc1snq9k7miiff88wnqg9gc6wxwbnypksgimw4xi9frcaa8kqi";
  };
  "bcm47xx"."mips74k" = {
    sha256 = "0c79wsxi0a8pyijyvlb1b204cx39z73cwa7zhrhy232kjq9bd3g3";
    feedsSha256.base = "0bpbnk44qkph2qh15izd5vjiv6pbb2bqcsnpk1zxy55ldwc3gwxa";
    feedsSha256.packages = "1m1s254lf7x604pxgp3zkj517ka3sqchl1d4z2fr7fcx2whjvsi7";
    feedsSha256.routing = "1yi3jmggsv8r0nzl0waqpigyhcc58s21p1qnbs52can888191cz6";
    feedsSha256.telephony = "1knhqcncynmf2nhkacssadsq3vfqzypd926g91fkc9s6knjywhdq";
  };
  "bcm27xx"."bcm2711" = {
    sha256 = "099gzj12jbk1fc1xdqp84ykmwph7d142baaikzjzfy07igjxl2i4";
    feedsSha256.base = "15n5g61dpgrrlkzb4kvilf5py0n5yjiswwd6mzd1aa6301pbnzqj";
    feedsSha256.packages = "0cxmk0kplg3kn6pkcjr6ynm97y0l9gxi56nl0if368c9i25c4qgy";
    feedsSha256.routing = "1jlnfzka13mn9v72rf7k6iy0qv2kq94gqr7z3xh8cbcin3lh6ig6";
    feedsSha256.telephony = "1y61yzx418wlmbr0jc0fj551qz67inx63sg09qdf2sj8j1j0xj0x";
  };
  "bcm27xx"."bcm2708" = {
    sha256 = "0mcaczzcm27vkqm7vznl86vdj3fxnsfivssh3f8f14bzslj45215";
    feedsSha256.base = "0ywkzcky4dk4sk5a3i8mlph3nr9y4ync14rz9b1cdmwksjh8x56p";
    feedsSha256.packages = "1czk70hdcdhlja8h0p53974gnzlsb49akb1shqwrnfwd03bjyvx5";
    feedsSha256.routing = "0cjkyvs9x9p5l1iid7gbvd06ipmr7xsspm9cdy1g7zqc25xi01yp";
    feedsSha256.telephony = "1d27aznbv2cyv2hz23bhs8gf3wv9wkd11gsnc662jml8fc2fjq0k";
  };
  "bcm27xx"."bcm2709" = {
    sha256 = "0fygnb03xy1j24knrsxddby086z58z48nkpx9k4jm8i99j724382";
    feedsSha256.base = "0bln5kfxsmb3aqbkmkkay91k3xb0xqjqcbg77cwr3cvndvnn0kw3";
    feedsSha256.packages = "1zgbx08a6llcb46rhy7d3bsa1zyjcwplxayy0yyk99yhq3ibmsq7";
    feedsSha256.routing = "19d4yckpdhcg65z0kiapqgs12mnk8dva77wqb28q1svwjj83p26q";
    feedsSha256.telephony = "0kizbchfa9zss3p4adi15gm7splkxqkss6z2yy6m8ih22jlsxgwd";
  };
  "bcm27xx"."bcm2710" = {
    sha256 = "1pli4zrc1fxbl13n9dwhil57qrzpq3dplk9jdjw2r8cv8w77sr79";
    feedsSha256.base = "1vf5vagbl99y9g3c2fhhxwkjvgdzy3ivrgw87wcvrq6dhyxm4sma";
    feedsSha256.packages = "14nj7l3w3mihmkwrcx48qiixwd4wwnk7vmclrm4gaxlz98vsf4rc";
    feedsSha256.routing = "1g9ad0125na9z8wbwrx0i016f5rv37967wp3li14p8lrysp393kp";
    feedsSha256.telephony = "0sv1fjybwky6d6frx7d64i2y6dlzvvsq7sn46rv8jx8lpk4yba47";
  };
  "mvebu"."cortexa53" = {
    sha256 = "1639bn8y03la5ypv4qn2yyh7l3kq5dxqpzr270cpk5x221z90m3p";
    feedsSha256.base = "1vf5vagbl99y9g3c2fhhxwkjvgdzy3ivrgw87wcvrq6dhyxm4sma";
    feedsSha256.packages = "14nj7l3w3mihmkwrcx48qiixwd4wwnk7vmclrm4gaxlz98vsf4rc";
    feedsSha256.routing = "1g9ad0125na9z8wbwrx0i016f5rv37967wp3li14p8lrysp393kp";
    feedsSha256.telephony = "0sv1fjybwky6d6frx7d64i2y6dlzvvsq7sn46rv8jx8lpk4yba47";
  };
  "mvebu"."cortexa72" = {
    sha256 = "0cpzdx8nivpgz5s4abajqmsk5lxm9dmf0habhgylp16vsyhixbdw";
    feedsSha256.base = "15n5g61dpgrrlkzb4kvilf5py0n5yjiswwd6mzd1aa6301pbnzqj";
    feedsSha256.packages = "0cxmk0kplg3kn6pkcjr6ynm97y0l9gxi56nl0if368c9i25c4qgy";
    feedsSha256.routing = "1jlnfzka13mn9v72rf7k6iy0qv2kq94gqr7z3xh8cbcin3lh6ig6";
    feedsSha256.telephony = "1y61yzx418wlmbr0jc0fj551qz67inx63sg09qdf2sj8j1j0xj0x";
  };
  "mvebu"."cortexa9" = {
    sha256 = "17kjhh74fbcqb7wr5iidaa9sz2q084sqsq30k3bbdgxlqy0qcdxy";
    feedsSha256.base = "1yaf8g96jalkvm8dfwfi4jcrxwyjx3a9xq58v084s2a2nqr3jkx9";
    feedsSha256.packages = "1yg0vxwnfphwl702r0fv9icvx0ikymzvfg8sshrl8n95d9f2wc5y";
    feedsSha256.routing = "10lslv2kjp7spv6586381kizr0f8l9zayq2cp9fk5q8hy15chrlr";
    feedsSha256.telephony = "1150v2yp91jb6v6qwim9lybk74x1kxcp86qd4hvsqqw4w3fmmfg0";
  };
  "at91"."sam9x" = {
    sha256 = "1r7hl0lqgkzad8wg8i0gdysakf4qhrm2q17giawz710nvd3zkglf";
    feedsSha256.base = "0w260by0h1qc9zbdfsv2cxs36fsqj2fp9rfmfpcy2pjv90hr73kb";
    feedsSha256.packages = "1dm07h5cb8pxz3mbhpd4l7xz4z8vxkv8qis11zh1jmg245n7gvd8";
    feedsSha256.routing = "1v2619dvv8bfizgaq88x7i6i9v7432g7brghydhpbwn2zwzzgpby";
    feedsSha256.telephony = "0pwnsb7xvygl4iqhsdva3a5qjs08xrf66g83klgyqibxkkjq0yad";
  };
  "at91"."sama5" = {
    sha256 = "1la3m04rkd8rrqs2gpnipdip2fbz41vbd9kp21q2d5kqzldynwwc";
    feedsSha256.base = "1y93az14hq9ry9gcaxyfmshzqbgqacxhbad3pga9ggqlycjkz6xl";
    feedsSha256.packages = "03fw35qz4yqs8iwvkzlv2zx51qpk5dwrph5sj1421bpzh7bglrvs";
    feedsSha256.routing = "15pjzqh7k13zqzgrb2hdfxyx99ssqrdsbcj5nqqx0cpk37p0s8r3";
    feedsSha256.telephony = "1f2laxbwlcspv2ydz7z5kcjgsdv33f67vjiqs6dibqkrmvhi86qg";
  };
  "gemini"."generic" = {
    sha256 = "1z82mr6ky7dd4178w1356nvv2b5zlypdkb8q6ln7ckp9a0ghbnxq";
    feedsSha256.base = "114kmnaq9g5n9vbd7qdh6dxdikphln25r95h5xa5p0r092jx02la";
    feedsSha256.packages = "1xnr2knfyxk5h62rdxlpyg1ja9hx1xbhlv7x02vn81xazzc4qbjn";
    feedsSha256.routing = "0jwxs74ryfqf4jvmyznpf5zhqrvrad378s7y64qzqagw104cvrp1";
    feedsSha256.telephony = "1bzdzcaqhlya8khs5cg3f267q5b58i38zvwsj3qidm7kkn5p6pn7";
  };
  "octeontx"."generic" = {
    sha256 = "04mfpgnq5nnpiqr44a7n6g5k9b3ggcdv55jb14fqs7ac81i9sylz";
  };
  "ipq40xx"."generic" = {
    sha256 = "15dgkx1n4yrkfi132ma2flf6aj3va47h6knz9ajm4a84fkricszz";
    feedsSha256.base = "0bln5kfxsmb3aqbkmkkay91k3xb0xqjqcbg77cwr3cvndvnn0kw3";
    feedsSha256.packages = "1zgbx08a6llcb46rhy7d3bsa1zyjcwplxayy0yyk99yhq3ibmsq7";
    feedsSha256.routing = "19d4yckpdhcg65z0kiapqgs12mnk8dva77wqb28q1svwjj83p26q";
    feedsSha256.telephony = "0kizbchfa9zss3p4adi15gm7splkxqkss6z2yy6m8ih22jlsxgwd";
  };
  "ipq40xx"."mikrotik" = {
    sha256 = "1chbplkwd0h430a8dj61krzsyxrzv4dsz0dbdm2qdy7v1yiji9bs";
    feedsSha256.base = "0bln5kfxsmb3aqbkmkkay91k3xb0xqjqcbg77cwr3cvndvnn0kw3";
    feedsSha256.packages = "1zgbx08a6llcb46rhy7d3bsa1zyjcwplxayy0yyk99yhq3ibmsq7";
    feedsSha256.routing = "19d4yckpdhcg65z0kiapqgs12mnk8dva77wqb28q1svwjj83p26q";
    feedsSha256.telephony = "0kizbchfa9zss3p4adi15gm7splkxqkss6z2yy6m8ih22jlsxgwd";
  };
  "mediatek"."mt7623" = {
    sha256 = "1zqb70ik7scj85c3hxlm6q5arp9p90l49ynwcxk81fasn7nskn8i";
    feedsSha256.base = "0bln5kfxsmb3aqbkmkkay91k3xb0xqjqcbg77cwr3cvndvnn0kw3";
    feedsSha256.packages = "1zgbx08a6llcb46rhy7d3bsa1zyjcwplxayy0yyk99yhq3ibmsq7";
    feedsSha256.routing = "19d4yckpdhcg65z0kiapqgs12mnk8dva77wqb28q1svwjj83p26q";
    feedsSha256.telephony = "0kizbchfa9zss3p4adi15gm7splkxqkss6z2yy6m8ih22jlsxgwd";
  };
  "mediatek"."mt7629" = {
    sha256 = "1gkwcdaazjhw3malkzbbylafsxv1p1xy80mbivmwxadral41m0sx";
    feedsSha256.base = "09mmqqcz7n83s0zhwcsp0mwxc7m0c9a1rv97i5y36g0njyv618yw";
    feedsSha256.packages = "1k8585i1nfmynj4bzkhh8qhk4lqi0fks8vy0xdby15jwgz7qqqsn";
    feedsSha256.routing = "0nqqnqsj9mxhsxifn6q3gn6nvdh59b7ix08m3n7vjdhbrrvnacv1";
    feedsSha256.telephony = "0rc316hlv8a56b7g2i1l674aakbfpmcjm8wxflsaq575v05zh7am";
  };
  "mediatek"."mt7622" = {
    sha256 = "1x29wma4j96lyjrw286qqvcs3xpgcd4lz806r2hfwfnqiff07b1l";
    feedsSha256.base = "1vf5vagbl99y9g3c2fhhxwkjvgdzy3ivrgw87wcvrq6dhyxm4sma";
    feedsSha256.packages = "14nj7l3w3mihmkwrcx48qiixwd4wwnk7vmclrm4gaxlz98vsf4rc";
    feedsSha256.routing = "1g9ad0125na9z8wbwrx0i016f5rv37967wp3li14p8lrysp393kp";
    feedsSha256.telephony = "0sv1fjybwky6d6frx7d64i2y6dlzvvsq7sn46rv8jx8lpk4yba47";
  };
  "malta"."be" = {
    sha256 = "11s2jzshvcrc04sxj5slnhwynjphc6sfg7v16camgi9a1hk0scf1";
  };
  "rockchip"."armv8" = {
    sha256 = "0whfn264zxdbghxjcqwgqiqhdn0l23ihj4iiji95kjszan1xd370";
    feedsSha256.base = "0rzw5ik81v0xxvifyjbv45mw8g8wnai5abf4n9igij1ai01n3ygn";
    feedsSha256.packages = "0p13vd0lnlcx353q10wd9120j4aw0qh69q7a0f115cainvfh7cbn";
    feedsSha256.routing = "18d4b3nla30hzifmp8n87wmhjxgqmc3392xdklz8sgrjm1ha9fsn";
    feedsSha256.telephony = "02fcayzhzlp4krdxyvyanc8ric1qbjcvg04yz0gm6h68ln58a3rw";
  };
  "ipq806x"."generic" = {
    sha256 = "16zr0ji1ph3r45vaw0nsrvm8qi9d6dp362byi9z8yd6iwjwjbxjk";
    feedsSha256.base = "0zfvnb28q5804a3dv813fvzg5yj0xm4sglnz174dbn9bi2csivmd";
    feedsSha256.packages = "0wk02zzpkwxg5w55cfx6dsfy0p0mmm7f0r3kvnvf9411pblpm4ck";
    feedsSha256.routing = "1lbhjwqwgkz669gpmb7ikgsj2x7rypiab542lvdbxphg5a92730p";
    feedsSha256.telephony = "0fmvdinn3asaarw08pypry40w13c9vd4am8r96f6f4gx3ya6spra";
  };
  "sunxi"."cortexa8" = {
    sha256 = "1nxaxrb1dl219xpsjk8pwby41l4y0px7kaf1a1r1csqx2yv677c5";
    feedsSha256.base = "00ar0d6nmqm011mx58k22x58gym5jh7rjfrv9vb0wgmc6ppf3pxj";
    feedsSha256.packages = "0zhihll3nldhjdxxz8mqpafbcz4ycadjmwkm565ln106chg4zk18";
    feedsSha256.routing = "1bfbmhf1j65k1g71sp5b6sa3vh5f3r13vsyf9yf4i57x1ny55ac8";
    feedsSha256.telephony = "1jnsjkvmqmydqx1sxac01l3cm2410azavmplvpjak6snmyi2wpg9";
  };
  "sunxi"."cortexa53" = {
    sha256 = "0mj5pfbzkaamrvkvc93dl8kfsp63p7q0hhqcj6prxmyp22krzrcp";
    feedsSha256.base = "1vf5vagbl99y9g3c2fhhxwkjvgdzy3ivrgw87wcvrq6dhyxm4sma";
    feedsSha256.packages = "14nj7l3w3mihmkwrcx48qiixwd4wwnk7vmclrm4gaxlz98vsf4rc";
    feedsSha256.routing = "1g9ad0125na9z8wbwrx0i016f5rv37967wp3li14p8lrysp393kp";
    feedsSha256.telephony = "0sv1fjybwky6d6frx7d64i2y6dlzvvsq7sn46rv8jx8lpk4yba47";
  };
  "sunxi"."cortexa7" = {
    sha256 = "0qsbsqnkclps1k058ykr9fxs9b6mxb6gxprxrcs05ym9if7kh689";
    feedsSha256.base = "0bln5kfxsmb3aqbkmkkay91k3xb0xqjqcbg77cwr3cvndvnn0kw3";
    feedsSha256.packages = "1zgbx08a6llcb46rhy7d3bsa1zyjcwplxayy0yyk99yhq3ibmsq7";
    feedsSha256.routing = "19d4yckpdhcg65z0kiapqgs12mnk8dva77wqb28q1svwjj83p26q";
    feedsSha256.telephony = "0kizbchfa9zss3p4adi15gm7splkxqkss6z2yy6m8ih22jlsxgwd";
  };
  "mpc85xx"."p1010" = {
    sha256 = "13hsw5aqdprsxh6pi4fx14iivjmkpaya0mrx6x7v1nxgc7w562aj";
    feedsSha256.base = "0rx1kdr8lngvw4zwc279k209ka6b3g70dsfflcsldl4kighxrqbj";
    feedsSha256.packages = "0lakqri659ka1nxmq6qcbsxfhh76vsn9zl63ya3byfrqqzhsci0m";
    feedsSha256.routing = "1mkr3frlyxyw8vgpk0rpp3iq818k93ka468hlwbr78p6g0kvs7cm";
    feedsSha256.telephony = "13n33bdjrx7pn4bhnpx14fqxiimgqan0mgyy2xgpbw0p78rlni83";
  };
  "mpc85xx"."p2020" = {
    sha256 = "11mba7zbigk1hymp6d2zfmrhh8irm88zca11f5hcadm2gvmssvxc";
    feedsSha256.base = "0rx1kdr8lngvw4zwc279k209ka6b3g70dsfflcsldl4kighxrqbj";
    feedsSha256.packages = "0lakqri659ka1nxmq6qcbsxfhh76vsn9zl63ya3byfrqqzhsci0m";
    feedsSha256.routing = "1mkr3frlyxyw8vgpk0rpp3iq818k93ka468hlwbr78p6g0kvs7cm";
    feedsSha256.telephony = "13n33bdjrx7pn4bhnpx14fqxiimgqan0mgyy2xgpbw0p78rlni83";
  };
  "mpc85xx"."p1020" = {
    sha256 = "1a89vi0g610ph2y1ggpcnmbf72zsr7r7a2iyinwjhhbyd0czic6k";
    feedsSha256.base = "0rx1kdr8lngvw4zwc279k209ka6b3g70dsfflcsldl4kighxrqbj";
    feedsSha256.packages = "0lakqri659ka1nxmq6qcbsxfhh76vsn9zl63ya3byfrqqzhsci0m";
    feedsSha256.routing = "1mkr3frlyxyw8vgpk0rpp3iq818k93ka468hlwbr78p6g0kvs7cm";
    feedsSha256.telephony = "13n33bdjrx7pn4bhnpx14fqxiimgqan0mgyy2xgpbw0p78rlni83";
  };
  "x86"."generic" = {
    sha256 = "1lixvas64qddf4ix2afzwpig9qsk0b0l76x2nl4490c4q3hzhv9w";
    feedsSha256.base = "1yw8d3y4czqxsvpjip1d57pw16g56sjb2b89jcjpd16bpklqbx21";
    feedsSha256.packages = "1gqj9ll99pi8w83q10mwan7i2p8xn47inlh0zhxnc9wp1i8sykyz";
    feedsSha256.routing = "1cr3yxfqi4acdmbavfjvdykkmr5hm5b0zcr6w2kplm7s90yhx8hm";
    feedsSha256.telephony = "1fmmxwkdy0ybnp3r22sm93m70rdwgsd2kb469awwr6425wawnn81";
  };
  "x86"."legacy" = {
    sha256 = "15zlh87121pn0q2hcxdi230akx9xfd5sjkr7bsszign1ha4mrdbk";
    feedsSha256.base = "1dbzhw3dy80qmkh3c62n8g3gvsdq88p088ad5f310bq28g8vcy4a";
    feedsSha256.packages = "1j6mph09piy0rk6gyii6p985rj6hzk9xg589n9wb3rb0w008994a";
    feedsSha256.routing = "08xpz38rb9h8knxhgc4059z9z0wv54dm61kmzdiv2b2rf1sir10c";
    feedsSha256.telephony = "1xb3wwfjy330200v7kmvqf072dq7w4dvp1wydzdlp7psm2mlpd4q";
  };
  "x86"."geode" = {
    sha256 = "1ig0n19j9j8mfnd8zffxxx7ckxccmk5k8s1k7vwsk2pqmq9qxspk";
    feedsSha256.base = "1dbzhw3dy80qmkh3c62n8g3gvsdq88p088ad5f310bq28g8vcy4a";
    feedsSha256.packages = "1j6mph09piy0rk6gyii6p985rj6hzk9xg589n9wb3rb0w008994a";
    feedsSha256.routing = "08xpz38rb9h8knxhgc4059z9z0wv54dm61kmzdiv2b2rf1sir10c";
    feedsSha256.telephony = "1xb3wwfjy330200v7kmvqf072dq7w4dvp1wydzdlp7psm2mlpd4q";
  };
  "x86"."64" = {
    sha256 = "0shz23fgh8yjrlw3d8zd3m87hc5pgv3mq8iqdyj02llxwvajwx9w";
    feedsSha256.base = "1xmwr9jyxmsw90mcq1qyqs6psq88bw22csn9ivyvibkn95aw508w";
    feedsSha256.packages = "102dkmvkwid6vg5i7warsx5k842p88pkdjjl551h70qnfw4cxm2y";
    feedsSha256.routing = "04l4799bjw632akqd7k8zk4cqm3yn1cdwzwjv0bwncrp0h5sh1ml";
    feedsSha256.telephony = "0hfjscbzvsq00bw0a3n9bxammql074yb9331f2a04iy9z1aiw28j";
  };
  "realtek"."generic" = {
    sha256 = "0frb6ppcn74s2ckaxrprk11prrg65f384bfcdad9bbf3qjfcn4al";
    feedsSha256.base = "1n3bvkh175y4n412qc2n4p8j5xkca59vivsww99vypql64hl5yfr";
    feedsSha256.packages = "088503xqnwmxkbfqnp8cdv4imywxbmq9wv6vw8wn6r83mrybf7k0";
    feedsSha256.routing = "0jbzfy14f9bdgnq0l7xpkbgnl554r35ql6pnjsbxfnslmzbjwjzb";
    feedsSha256.telephony = "1na7n0xl0xqv1q2kmlrwz5n1qxskmy2mylgmrvisihmnswivjrsb";
  };
  "armvirt"."32" = {
    sha256 = "0q8l5zd813sdxbv0cngmx3cqqz5pnkcrhrl2bphibzh89s8shh24";
  };
  "armvirt"."64" = {
    sha256 = "1zbpd93bz21ad80k8z7i4dvhpbmyfgdqdanxxz3dacl4zy7a91sc";
  };
  "kirkwood"."generic" = {
    sha256 = "09zpbhz4nhicsm54x5nimknsfpyjc7hjqiklk730zrlmpr07hg8g";
    feedsSha256.base = "1i9q511r6jlalbipil6r84nz14zj3ydwclgn3dmdgd6p0cl4wjiq";
    feedsSha256.packages = "0mj4ziqrcx28kqbbrx8c2b764h4zh7b9ylapzaiq48mmggirdi6f";
    feedsSha256.routing = "18n0y83r760p5np7ndy3wdc69iv90xya31aq90gdaxnnfy4vcb3a";
    feedsSha256.telephony = "1r7dychcrn668df71l0l0shjb1ir4jxwx4794yx0as6hxpqllv7n";
  };
  "ath79"."generic" = {
    sha256 = "01vyvxkphnlpvnw2az8ficzzjn3zjvwlvkp0hpicy8w94ih1fxdj";
    feedsSha256.base = "0wiwp11d1vmxbxkhrirs4gnwd7akb861pz3mzv03c7ypdwg49q8r";
    feedsSha256.packages = "1gsg4n74z0v01wsil5skhd0ngd75sjyxy7qvv4ghymp6fk8wb02s";
    feedsSha256.routing = "0kf85giy0bins01a5cl8lgmch6mrddivk43as2vmp66xqq6ksldw";
    feedsSha256.telephony = "1wn34c10wk5axb63035pzp4lkhk156sv6fjz4ir6nrdga78jsj8j";
  };
  "ath79"."mikrotik" = {
    sha256 = "1b0ljsb72xkmx8d9cfk2r8jv3bs5adj6b2hnhdiqls2iaz6al3yq";
    feedsSha256.base = "0wiwp11d1vmxbxkhrirs4gnwd7akb861pz3mzv03c7ypdwg49q8r";
    feedsSha256.packages = "1gsg4n74z0v01wsil5skhd0ngd75sjyxy7qvv4ghymp6fk8wb02s";
    feedsSha256.routing = "0kf85giy0bins01a5cl8lgmch6mrddivk43as2vmp66xqq6ksldw";
    feedsSha256.telephony = "1wn34c10wk5axb63035pzp4lkhk156sv6fjz4ir6nrdga78jsj8j";
  };
  "ath79"."nand" = {
    sha256 = "00775cbgvp6xgwdvmba0abgisjd5q9srnqiyhd71qswlynzai3p7";
    feedsSha256.base = "0wiwp11d1vmxbxkhrirs4gnwd7akb861pz3mzv03c7ypdwg49q8r";
    feedsSha256.packages = "1gsg4n74z0v01wsil5skhd0ngd75sjyxy7qvv4ghymp6fk8wb02s";
    feedsSha256.routing = "0kf85giy0bins01a5cl8lgmch6mrddivk43as2vmp66xqq6ksldw";
    feedsSha256.telephony = "1wn34c10wk5axb63035pzp4lkhk156sv6fjz4ir6nrdga78jsj8j";
  };
  "ath79"."tiny" = {
    sha256 = "1271frbb1vdl13iz6q212fkmbq9c3r237x62ywjb3s4v92rbrc9s";
    feedsSha256.base = "0wiwp11d1vmxbxkhrirs4gnwd7akb861pz3mzv03c7ypdwg49q8r";
    feedsSha256.packages = "1gsg4n74z0v01wsil5skhd0ngd75sjyxy7qvv4ghymp6fk8wb02s";
    feedsSha256.routing = "0kf85giy0bins01a5cl8lgmch6mrddivk43as2vmp66xqq6ksldw";
    feedsSha256.telephony = "1wn34c10wk5axb63035pzp4lkhk156sv6fjz4ir6nrdga78jsj8j";
  };
  "archs38"."generic" = {
    sha256 = "0lwgvp5qh6akr65z0msfqxzkhp9ja3s8ip9gxpiqpg8689jcabac";
    feedsSha256.base = "0xz1w7dwgmspn04wn5dkp27g1hhqk7vzbg3xb65m8szd2si904qr";
    feedsSha256.packages = "05p85zmkbh3y8y98fapnhnyxyhhklrg6znxkmr1k9pdv3jmizbjn";
    feedsSha256.routing = "0fngh6g56cndz4njlgldrwxfm95brcbp6b7plqm85lvwybq6b9l9";
    feedsSha256.telephony = "14g5fy09h3mn4jiqb3hz4isddgcjas25gr1pf695bjry6m9maycd";
  };
  "ath25"."generic" = {
    sha256 = "1yv5iyz14i0i7n2gr376bxr6qs5v2qy7a3xshk0f5l1asd9kh64a";
    feedsSha256.base = "08lwlj32kym9gwfk2h1mg96b9rvpcrwq2cvpkxlj34c9dfz900sj";
    feedsSha256.packages = "0fldjf2abihip3ij22wcwlyrjfxhm1m9my16vb5qlq4msmfnb07i";
    feedsSha256.routing = "15dnpjk62y540brhxm31rcznka03kwjjkhlbmn337clqz0n84gax";
    feedsSha256.telephony = "1xvdgps6b5icwlppi7fjk4c0jnyy40nzb6mjz47h8abzjbc72fmz";
  };
  "apm821xx"."nand" = {
    sha256 = "0r202z8q18v2afjfgb9yp9glpy78qbxvlxjjzwbwac47ab8jvd3h";
    feedsSha256.base = "07xfp3yk62pqay5pr631gi1f11g744ql2qsw2zrpiqas620015q8";
    feedsSha256.packages = "0bib9zw10hy91j3cp7nnpc4wdhvrymw2sf2jhkcvri1qs32aznfh";
    feedsSha256.routing = "16hbzlzh11cn9qbgv1nxapj0gd6dbpgs66xar6qycq1lzv0h31i7";
    feedsSha256.telephony = "14q9yhh15kg0w16pfwy2kj67zcxz3swwnxw0mpgdsm9whfqv0n0h";
  };
  "apm821xx"."sata" = {
    sha256 = "05iv7wgbxf06xyb94kyiz536kcjjhmmb0id0qlx5vq1z7kphs8ik";
    feedsSha256.base = "07xfp3yk62pqay5pr631gi1f11g744ql2qsw2zrpiqas620015q8";
    feedsSha256.packages = "0bib9zw10hy91j3cp7nnpc4wdhvrymw2sf2jhkcvri1qs32aznfh";
    feedsSha256.routing = "16hbzlzh11cn9qbgv1nxapj0gd6dbpgs66xar6qycq1lzv0h31i7";
    feedsSha256.telephony = "14q9yhh15kg0w16pfwy2kj67zcxz3swwnxw0mpgdsm9whfqv0n0h";
  };
  "bcm53xx"."generic" = {
    sha256 = "1sgbkfb37qgx7kblkzwhn42jxwm71v3q1jnmayr00lmsf37amrmp";
    feedsSha256.base = "0hkgam5gay18r64gmq41kwr20k4wc6s7vjv2cf77bgp5jz6n7nk6";
    feedsSha256.packages = "1i68v7f0m37fdzv08ial2bqc668n1vhdd8gipv7pzviqgvyvws27";
    feedsSha256.routing = "07z95qihddzjkds8c7avnlsh81lcnny5f3374bm5b19l7vr9sys6";
    feedsSha256.telephony = "0cp14myz0khi8mv76v276a0zmhs0hxfyp09li15g1xfz27zrpf53";
  };
  "ramips"."mt76x8" = {
    sha256 = "10drsh4sb23w4fn041lrdqs3rvdhqyxwdka557vd2y5hjcs61z33";
    feedsSha256.base = "12pnzzhqzc8n9ix12yvlfycql8ig1y07pxv7flk4wgrliap1glxj";
    feedsSha256.packages = "175xqnqwawy5y13j08xg56wa87l3k5bk1vhl700rqs2f4p9yy2y7";
    feedsSha256.routing = "0ac9xcd2mld3a171nlzfn1zdpl5wc96zq1nx9gh81ppcnx2i6sp1";
    feedsSha256.telephony = "1cpia037ifkg7sgdmcykqzw7d61654g5kg07iyixpi9z6r06mk4l";
  };
  "ramips"."mt7620" = {
    sha256 = "0ilfc3mm0j7c7ckahyfcmynzp8hs7g0zfyh7vlijnbv4rkdydfms";
    feedsSha256.base = "12pnzzhqzc8n9ix12yvlfycql8ig1y07pxv7flk4wgrliap1glxj";
    feedsSha256.packages = "175xqnqwawy5y13j08xg56wa87l3k5bk1vhl700rqs2f4p9yy2y7";
    feedsSha256.routing = "0ac9xcd2mld3a171nlzfn1zdpl5wc96zq1nx9gh81ppcnx2i6sp1";
    feedsSha256.telephony = "1cpia037ifkg7sgdmcykqzw7d61654g5kg07iyixpi9z6r06mk4l";
  };
  "ramips"."rt3883" = {
    sha256 = "05ai7ja94dff49lvvwslkjldnbx1dh93ypiii272cw7b5a4r0ilv";
    feedsSha256.base = "0bpbnk44qkph2qh15izd5vjiv6pbb2bqcsnpk1zxy55ldwc3gwxa";
    feedsSha256.packages = "1m1s254lf7x604pxgp3zkj517ka3sqchl1d4z2fr7fcx2whjvsi7";
    feedsSha256.routing = "1yi3jmggsv8r0nzl0waqpigyhcc58s21p1qnbs52can888191cz6";
    feedsSha256.telephony = "1knhqcncynmf2nhkacssadsq3vfqzypd926g91fkc9s6knjywhdq";
  };
  "ramips"."rt305x" = {
    sha256 = "1jaln3afzgfp6h5vfx1aka0bj3hm076zbnnhfixfxlyw9qd13knf";
    feedsSha256.base = "12pnzzhqzc8n9ix12yvlfycql8ig1y07pxv7flk4wgrliap1glxj";
    feedsSha256.packages = "175xqnqwawy5y13j08xg56wa87l3k5bk1vhl700rqs2f4p9yy2y7";
    feedsSha256.routing = "0ac9xcd2mld3a171nlzfn1zdpl5wc96zq1nx9gh81ppcnx2i6sp1";
    feedsSha256.telephony = "1cpia037ifkg7sgdmcykqzw7d61654g5kg07iyixpi9z6r06mk4l";
  };
  "ramips"."mt7621" = {
    sha256 = "1g9fznv8m7p45z68p9y95gglairfq6wxr1mw3jiy3r7n111qxdjz";
    feedsSha256.base = "12pnzzhqzc8n9ix12yvlfycql8ig1y07pxv7flk4wgrliap1glxj";
    feedsSha256.packages = "175xqnqwawy5y13j08xg56wa87l3k5bk1vhl700rqs2f4p9yy2y7";
    feedsSha256.routing = "0ac9xcd2mld3a171nlzfn1zdpl5wc96zq1nx9gh81ppcnx2i6sp1";
    feedsSha256.telephony = "1cpia037ifkg7sgdmcykqzw7d61654g5kg07iyixpi9z6r06mk4l";
  };
  "ramips"."rt288x" = {
    sha256 = "0m20iwqq17x6zjrh0dildzrl39mj9g61m800i37vj2r3sa3xk9iy";
    feedsSha256.base = "12pnzzhqzc8n9ix12yvlfycql8ig1y07pxv7flk4wgrliap1glxj";
    feedsSha256.packages = "175xqnqwawy5y13j08xg56wa87l3k5bk1vhl700rqs2f4p9yy2y7";
    feedsSha256.routing = "0ac9xcd2mld3a171nlzfn1zdpl5wc96zq1nx9gh81ppcnx2i6sp1";
    feedsSha256.telephony = "1cpia037ifkg7sgdmcykqzw7d61654g5kg07iyixpi9z6r06mk4l";
  };
  "lantiq"."xrx200" = {
    sha256 = "1w59gx50x6gpgqbgrkg4q9wx1ggcfgjavdhlfizicm4h5kcx801d";
    feedsSha256.base = "0wiwp11d1vmxbxkhrirs4gnwd7akb861pz3mzv03c7ypdwg49q8r";
    feedsSha256.packages = "1gsg4n74z0v01wsil5skhd0ngd75sjyxy7qvv4ghymp6fk8wb02s";
    feedsSha256.routing = "0kf85giy0bins01a5cl8lgmch6mrddivk43as2vmp66xqq6ksldw";
    feedsSha256.telephony = "1wn34c10wk5axb63035pzp4lkhk156sv6fjz4ir6nrdga78jsj8j";
  };
  "lantiq"."ase" = {
    sha256 = "0ik30srwyqnbmdr6zsy0kvc72hrm9dj76rdbbib038mcf4lgf594";
  };
  "lantiq"."xway" = {
    sha256 = "1yxsgymw0cvyhc7v53ckaqlprq2j8v2bs5dqi2kg8sq3zs25ngiw";
    feedsSha256.base = "0wiwp11d1vmxbxkhrirs4gnwd7akb861pz3mzv03c7ypdwg49q8r";
    feedsSha256.packages = "1gsg4n74z0v01wsil5skhd0ngd75sjyxy7qvv4ghymp6fk8wb02s";
    feedsSha256.routing = "0kf85giy0bins01a5cl8lgmch6mrddivk43as2vmp66xqq6ksldw";
    feedsSha256.telephony = "1wn34c10wk5axb63035pzp4lkhk156sv6fjz4ir6nrdga78jsj8j";
  };
  "lantiq"."xway_legacy" = {
    sha256 = "0pmn9vf46pz6lzs4wdlk56kkfjpf8c4xdwmv1zsr6mf0194j2761";
  };
  "octeon"."generic" = {
    sha256 = "1mgppg235wp46yyykcgcqzppsa3paxpw955a45q7gz0vk1vbsxj0";
    feedsSha256.base = "12wh815alnb0sm4lpxb9w5l0hnsjb04vxkwf090d4n9w3zr95rw9";
    feedsSha256.packages = "1prh29hvcwvc2nabl0i7kfvb1czlsvx0pjm53b9604p66wh95jkx";
    feedsSha256.routing = "1i8dmmp27p5xhb58fr54f1vfgrxyf4rzm99m1hh6agly2f950fpn";
    feedsSha256.telephony = "18kbzma6w9cb410y3fsrg6849hp6xr13ln197cahcg5siy3i8f4s";
  };
  "pistachio"."generic" = {
    sha256 = "1xdfxyfff4px410gb7p05q9ypq0snbc7kw817qipfxs3cv30aaxg";
    feedsSha256.base = "1anqbk4zf1x9az9gm87614ag8dsyxfi2j4wbbiqv2lw118ajm0xf";
    feedsSha256.packages = "1s98ixkajc85y5m7kv55zi0qhg7jm5v3ld2zkayxkl4rx0xp41yz";
    feedsSha256.routing = "1c1iysq198awl94xjavy23ccvb5v0a8jmpaxb4nn8966cspjaz42";
    feedsSha256.telephony = "008qs23vb637g234440fzrbwcq1h9m2mwinaxcd5f11129175drb";
  };
  "layerscape"."armv8_64b" = {
    sha256 = "1fv24wcx5m37xjlbmi9nabggjp1jb81pq9fajc6wlxzzlf3awmyd";
    feedsSha256.base = "0rzw5ik81v0xxvifyjbv45mw8g8wnai5abf4n9igij1ai01n3ygn";
    feedsSha256.packages = "0p13vd0lnlcx353q10wd9120j4aw0qh69q7a0f115cainvfh7cbn";
    feedsSha256.routing = "18d4b3nla30hzifmp8n87wmhjxgqmc3392xdklz8sgrjm1ha9fsn";
    feedsSha256.telephony = "02fcayzhzlp4krdxyvyanc8ric1qbjcvg04yz0gm6h68ln58a3rw";
  };
  "layerscape"."armv7" = {
    sha256 = "1b7zgfsgyllfc70c72zakvpn7bnnmlpznb7rnhpx2hz375ydh59a";
    feedsSha256.base = "0bln5kfxsmb3aqbkmkkay91k3xb0xqjqcbg77cwr3cvndvnn0kw3";
    feedsSha256.packages = "1zgbx08a6llcb46rhy7d3bsa1zyjcwplxayy0yyk99yhq3ibmsq7";
    feedsSha256.routing = "19d4yckpdhcg65z0kiapqgs12mnk8dva77wqb28q1svwjj83p26q";
    feedsSha256.telephony = "0kizbchfa9zss3p4adi15gm7splkxqkss6z2yy6m8ih22jlsxgwd";
  };
  "omap"."generic" = {
    sha256 = "089ykkvk6m9sbdm4y2ridb8pglbd7v37mcw9996yb9l5krv3sm91";
    feedsSha256.base = "00ar0d6nmqm011mx58k22x58gym5jh7rjfrv9vb0wgmc6ppf3pxj";
    feedsSha256.packages = "0zhihll3nldhjdxxz8mqpafbcz4ycadjmwkm565ln106chg4zk18";
    feedsSha256.routing = "1bfbmhf1j65k1g71sp5b6sa3vh5f3r13vsyf9yf4i57x1ny55ac8";
    feedsSha256.telephony = "1jnsjkvmqmydqx1sxac01l3cm2410azavmplvpjak6snmyi2wpg9";
  };
  "tegra"."generic" = {
    sha256 = "120di06j0x7pby3b5chq78glfn9yy2ndj6n4nmfjikmsi2iv243g";
    feedsSha256.base = "1yaf8g96jalkvm8dfwfi4jcrxwyjx3a9xq58v084s2a2nqr3jkx9";
    feedsSha256.packages = "1yg0vxwnfphwl702r0fv9icvx0ikymzvfg8sshrl8n95d9f2wc5y";
    feedsSha256.routing = "10lslv2kjp7spv6586381kizr0f8l9zayq2cp9fk5q8hy15chrlr";
    feedsSha256.telephony = "1150v2yp91jb6v6qwim9lybk74x1kxcp86qd4hvsqqw4w3fmmfg0";
  };
  "bcm4908"."generic" = {
    sha256 = "0z6js0vhd7qpgjck4qswqbd9kfayqicfd6n87hjl9hh5mjvcj6xq";
    feedsSha256.base = "1vf5vagbl99y9g3c2fhhxwkjvgdzy3ivrgw87wcvrq6dhyxm4sma";
    feedsSha256.packages = "14nj7l3w3mihmkwrcx48qiixwd4wwnk7vmclrm4gaxlz98vsf4rc";
    feedsSha256.routing = "1g9ad0125na9z8wbwrx0i016f5rv37967wp3li14p8lrysp393kp";
    feedsSha256.telephony = "0sv1fjybwky6d6frx7d64i2y6dlzvvsq7sn46rv8jx8lpk4yba47";
  };
}
